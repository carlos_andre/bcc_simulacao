// Esta função gera un grid de nlin por ncol
// para simular a população de uma localidade
// densidade é a probabilidade de cada célula estar ocupada
// prob de uma célula ocupada 

//Criação de estados para organizar o código.
estado_celula_vazia=1
estado_pessoa_sadia=2
estado_pessoa_doente=3
estado_pessoa_curada=4
estado_pessoa_falecida=5
estado_pessoa_vacinada = 6
estado_pessoa_mutacao = 7

function [grid,ndoentes] = gera_populacao_inicial(nlin,ncol,desidade,prob)
    //Gera a população inicial de acordo a função de probabilidade binomial
    grid = grand(nlin,ncol,'bin',1,desidade)
    //soma 1 em cada célula par poder visualizar
    grid = grid+1//alterei essa linha para melhorar a legibilidade do código
    nindividuos = length(find(grid==estado_pessoa_sadia))
    //pelo menos um doente na população
    ndoentes = max(1,int(nindividuos*prob))
    //seleciona aleatóriamente um indivíduos como doentes
    doentes = samwr(ndoentes,1,find(grid==estado_pessoa_sadia))
    grid(doentes) = estado_pessoa_doente
    //Pessoas com mutação
    pessoasComMutacao = samwr(nindividuos*0.05,1,find(grid==estado_pessoa_sadia))
    grid(pessoasComMutacao) = estado_pessoa_mutacao 
    //Pessoas vacinadas
    pessoasVacinadas = samwr(nindividuos*0.125,1,find(grid==estado_pessoa_sadia | grid==estado_pessoa_mutacao))
    grid(pessoasVacinadas) = estado_pessoa_vacinada 
endfunction


function [infectado] = foi_infectado(grid,x,y,prob)
    [nlin,ncol] = size(grid)
    vizinhos_infectados = 0
    for i=[-1,1]
        vizinho_x = modulo(x + i,nlin)
        vizinho_y = modulo(y + i,ncol)
        if vizinho_x == 0
            vizinho_x = nlin
        end
        if vizinho_y == 0
            vizinho_y = ncol
        end
        if grid(vizinho_x,y)==estado_pessoa_doente then
            vizinhos_infectados = vizinhos_infectados+1
        end
        if grid(x,vizinho_y)==estado_pessoa_doente then
            vizinhos_infectados = vizinhos_infectados+1
        end
    end

    if vizinhos_infectados > 0
        infectado = rand() < prob
    else
        infectado = %f
    end
endfunction

function [curado] = foi_curado(grid,x,y,prob)
    curado = rand() < prob
endfunction

function [falecido] = faleceu(grid,x,y,prob)
    falecido = rand() < prob
endfunction


nlin = 20;
ncol = 20;
densidade = 0.8;
inicial_infectaco = 0.02;
prob_infeccao = 0.8;
prob_infeccao_vacinado = 0.03;
prob_cura = 0.3;
prob_morte = 0.01;
numeroMortos = 0;
numeroCurados = 0;
[grid,numeroDoentes] = gera_populacao_inicial(nlin,ncol,densidade,inicial_infectaco)
printf("Dias\t|Sadios\t|Doentes|Mortos\t|Curados\n")
for iteracao = 1:100
    xtitle(sprintf("Dia %d",iteracao))
    if numeroDoentes > 0 then
        Matplot(grid)
        new_pop = grid
        for x=1:nlin
            for y=1:ncol
                if grid(x,y)==estado_pessoa_mutacao & foi_infectado(grid,x,y,prob_infeccao)
                    new_pop(x,y) = estado_pessoa_falecida
                    numeroMortos = numeroMortos +1
                end
                if grid(x,y)==estado_pessoa_sadia & foi_infectado(grid,x,y,prob_infeccao)
                    new_pop(x,y) = estado_pessoa_doente
                    numeroDoentes = numeroDoentes +1
                end
                if grid(x,y)==estado_pessoa_vacinada & foi_infectado(grid,x,y,prob_infeccao_vacinado)
                    new_pop(x,y) = estado_pessoa_doente
                    numeroDoentes = numeroDoentes +1
                end
                if grid(x,y)==estado_pessoa_doente & foi_curado(grid,x,y,prob_cura)
                    new_pop(x,y) = estado_pessoa_curada
                    numeroDoentes = numeroDoentes - 1
                    numeroCurados = numeroCurados + 1
                end
                if grid(x,y)==estado_pessoa_doente & new_pop(x,y)~=estado_pessoa_curada & faleceu(grid,x,y,prob_morte)
                    new_pop(x,y) = estado_pessoa_falecida
                    numeroDoentes = numeroDoentes - 1
                    numeroMortos = numeroMortos + 1
                end
            end
        end
        grid=new_pop
    end
    numeroSadios = length(find(grid==estado_pessoa_sadia))
    printf("%d\t|%d\t|%d\t|%d\t|%d\n",iteracao,numeroSadios,numeroDoentes,numeroMortos,numeroCurados)
end
