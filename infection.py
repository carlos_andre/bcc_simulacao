import numpy as np
import matplotlib.pyplot as plt
def GerarGridInicial( nlin, ncol, densidade, infeccaoInicial ):
    "Gera grid inicial para simulação"
    x=np.random.binomial( 1, densidade, (nlin*ncol))
    nIndividuos = np.count_nonzero(x)
    doentes = np.random.choice(np.where(x == 1)[0],round( nIndividuos*infeccaoInicial)) #Seleciona algumas pessoas para serem infectadas
    x[doentes] = 2
    return x.reshape((nlin,ncol))
grid = GerarGridInicial(50,50,.8,.02)
#y = np.sin(x)
plt.imshow(grid)
plt.show()
print(np.where(grid == 1))
#plt.matshow(x,1)